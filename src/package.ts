const pkg = require("../package.json");

export const name: string = pkg.name;
export const version: string = pkg.version;
export const homepage: string = pkg.homepage;
export const bugs: string = pkg.bugs.url;
